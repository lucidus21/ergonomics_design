"""[Notice]

Integer Coefficient:

To increase computational speed, the CP solvers work over the integers.
To use them, you must define your optimization problem using integers only.
If your problem has a constraint with non-integer coefficients,
you need to first multiply the entire constraint by a sufficiently large integer
so that the resulting constraint has all integer coefficients.
Do the same if the objective has non-integer coefficients.
These changes won't affect the optimal solution for the problem.
"""


import pandas as pd
import numpy as np
from ortools.sat.python import cp_model


with open("data/emojis.txt") as f:
    EMOJIS = f.read().strip().split("\n")
print("[CONSTANT] ordered EMOJIS: {}".format(list(enumerate(EMOJIS))))
with open("data/gestures.txt") as f:
    GESTURES = f.read().strip().split("\n")
print("[CONSTANT] ordered GESTURES: {}".format(list(enumerate(GESTURES))))
INTUITIVE_UNNORMALIZED_CSV = "data/intuitive_unnormalized.csv"
GESTURE_SIMILARITY_CSV = "data/gesture_similarity_upper_triangular.csv"
GESTURE_RFC_CSV = "data/gesture_rfc.csv"

# Variable naming template
X = "x_{}_{}"
ANY = "any"

# Large N for integer coefficient
LARGE_M = 1000
SAMPLE_N = 6
K_CONSTANT = 27
print("[CONSTANT] LARGE_M: {}".format(LARGE_M))
print("[CONSTANT] SAMPLE_N: {}".format(SAMPLE_N))


# { -- Data Manipulation
def load(path, index_col="emoji"):
    return pd.read_csv(path, index_col=index_col)


def load_with_zero(path, index_col="emoji"):
    return load(path, index_col).fillna(0)


def normalize_by_maxrow(df):
    return df.divide(df.max(axis=1), axis=0)


def normalize_by_samples(df):
    return df.divide(SAMPLE_N)


def multiply_big_integer(df):
    return (df * LARGE_M).applymap(lambda x: int(x))


def fill_diagonal_with_n(df, n=0):
    np.fill_diagonal(df.values, n)
    return df


def copy_upper_tri_to_lower_tri(df):
    matrix = df.copy().values
    assert len(df.columns) == len(df.index)
    N = len(df.columns)
    for i in range(N):
        for j in range(i, N):
            matrix[j][i] = matrix[i][j]
    return pd.DataFrame(matrix, index=df.index, columns=df.columns)

# Data Manipulation -- }


# {-- Utilities
def print_dataframe(df):
    print(df.to_string())


def print_xvalues(solver, x_dict):
    x_values = np.zeros((len(EMOJIS), len(GESTURES)))
    for e, emoji in enumerate(EMOJIS):
        for j, gesture in enumerate(GESTURES):
            x_values[e, j] = solver.Value(x_dict[X.format(e, j)])
    x_df = pd.DataFrame(x_values, columns=GESTURES, index=EMOJIS)
    print_dataframe(x_df)
    return x_df
# --}


def main():
    # Coefficient
    intuitive = multiply_big_integer(
        normalize_by_maxrow(load_with_zero(INTUITIVE_UNNORMALIZED_CSV))
    )
    assert list(intuitive.columns.values) == GESTURES
    assert list(intuitive.index.values) == EMOJIS
    print("[Coefficient] Unnormalized intuitive matrix: ")
    print_dataframe(intuitive)

    similiarity = normalize_by_samples(
        copy_upper_tri_to_lower_tri(load_with_zero(GESTURE_SIMILARITY_CSV, "gesture"))
    )
    assert np.diag(similiarity).sum() == 0  # Try to avoid dummy result
    assert list(similiarity.columns.values) == GESTURES
    assert list(similiarity.index.values) == GESTURES
    np.allclose(similiarity.transpose().values, similiarity.values)
    dissimiliarity = 1 - similiarity
    similiarity = multiply_big_integer(similiarity)
    dissimiliarity = multiply_big_integer(dissimiliarity)
    print("[Coefficient] Normalized similiarity matrix: ")
    print_dataframe(similiarity)
    print("[Coefficient] Normalized dissimiliarity matrix: ")
    print_dataframe(dissimiliarity)

    rfc = load(GESTURE_RFC_CSV, "gesture")
    assert list(rfc.columns.values) == ["repeat", "nonfreedom", "complexity"]
    assert list(rfc.index.values) == GESTURES

    # Model
    model = cp_model.CpModel()

    # Variable initialization
    x_meta = {}
    x = {}
    for e, emoji in enumerate(EMOJIS):
        for j, gesture in enumerate(GESTURES):
            x_name = X.format(e, j)
            x[x_name] = model.NewIntVar(0, 1, x_name)  # [0, 1]
            x_meta[x_name] = "<emoji:{} gesture:{}>".format(emoji, gesture)
            print("[Initialization] {}: 0, 1".format(x_name))

    # Constraints

    # Only one gesture for each emoji
    for e, emoji in enumerate(EMOJIS):
        model.Add(sum([x[X.format(e, j)] for j in range(len(GESTURES))]) == 1)
        print("[Constraint] Sum({}) = 1".format(X.format(e, ANY)))

    for e, emoji in enumerate(EMOJIS):
        sum_exclusive_gesture = 0
        for exclusive_gesture in filter(lambda gesture: emoji not in gesture, GESTURES):
            j = GESTURES.index(exclusive_gesture)
            sum_exclusive_gesture += x[X.format(e, j)]
        model.Add(sum_exclusive_gesture == 0)
        print("[Constraint] Sum(gesture of especially designed for this emoji: {}) = 0".format(emoji))

    # Objective Function
    Z1 = 0
    for e, emoji in enumerate(EMOJIS):
        for j, gesture in enumerate(GESTURES):
            Z1 += intuitive.values[e, j] * x[X.format(e, j)]
            print("[Z1] Z1 += {}(intuitive) * {}(x)".format(intuitive.values[e, j], x_meta[X.format(e, j)]))

    Z2 = 0
    for j in range(len(GESTURES)):
        dissim = dissimiliarity.values.min(axis=1)[j]
        Z2 += dissim * sum([x[X.format(e, j)] for e in range(len(EMOJIS))])
        print("[Z2] Z2 += {}(dissimiliarity[{}]) * sum(x_{}_{})".format(dissim, x_meta[X.format(e, j)], ANY, j))

    Z3 = 0
    for j, gesture in enumerate(GESTURES):
        assert rfc.index[j] == gesture
        comfort = int((K_CONSTANT - rfc.values[j].prod()) / K_CONSTANT * LARGE_M)
        Z3 += comfort * sum([x[X.format(e, j)] for e in range(len(EMOJIS))])
        print("[Z3] Z3 += {}(K - rfc[{}]) * sum(x_{}_{})".format(comfort, gesture, ANY, j))

    model.Maximize(Z1 + Z2 + Z3)

    solver = cp_model.CpSolver()
    status = solver.Solve(model)

    print()
    print("=" * 30)
    print()
    if status == cp_model.OPTIMAL:
        print('Maximum of objective function: %i' % solver.ObjectiveValue())
        print('Z1: %i' % solver.Value(Z1))
        print('Z2: %i' % solver.Value(Z2))
        print('Z3: %i' % solver.Value(Z3))
        df = print_xvalues(solver, x)
        print("Optimal GV: ")
        print(df.idxmax(axis=1))


if __name__ == '__main__':
    main()
