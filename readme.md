Ergonomics Design(406.326_001) 2018 Fall, Project1
==================================================

| Team4 Members |||
| --- | --- | --- |
| 2011-11949 이상준 | 2011-11927 신범준 | 2012-11842 진창언 |
| 2012-11760 심대보 | 2012-11865 황창현 | 2013-11267 김상엽 |


### Setup environments

- pyenv
- python 3.5.1

```
$ pyenv install 3.5.1
$ pyenv local 3.5.1
$ pyenv virtualenv 3.5.1 ortool
$ pyenv activate ortool
$ pip install -r requirements.txt
```

### How to test

```
$ python gesture.py # check example output in output/
```


### Troubleshootings

- If `protobuf` related error occurs, manual installation of protobuf is recommended
